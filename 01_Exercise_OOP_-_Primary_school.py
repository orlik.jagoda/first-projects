
class School:
    def __init__(self, school_type, number, city):
        self.school_type = school_type
        self.nuber = number
        self.city = city

    def __str__(self):
        return f"{self.school_type} number {self.nuber} in {self.city}"
    
class Classroom(School):
    def __init__(self, type_of_classroom, room_number, school):
        self.type_of_classroom = type_of_classroom
        self.room_number = room_number
        self.school = school
        self.group = []

    def speak(self):
        return f"{self.type_of_classroom} is located in {self.school}"

    def add_group(self, group):
        number_of_group = 1 
        if len(self.group) < number_of_group:
            self.group.append(group)
            group.classroom = self.group
            return True
        else:
            return f"This classroom is full"

    def classroom_description(self):
        return f"In this classromm is {len(self.group)} group"
        
class People_in_school:
    def __init__(self, profession, name, age):
        self.profession = profession
        self.name = name
        self.age = age

    def introduce(self):
        return f"My name is {self.name}, I'm {self.age} years old. I'm {self.profession}."

class Teacher(People_in_school):
    pass

class Group_of_students(People_in_school):
    def __init__(self, level, group, size_group, supervising_teacher):
        self.level = level
        self.group = group
        self.size_group = size_group
        self.supervising_teacher = supervising_teacher
        student_list = []
        self.students = student_list
                
    def group_description(self):
        return f"""Class {self.level}{self.group} has {len(self.students)} students.
Supervising teacher: {self.supervising_teacher.introduce()}
"""

    def add_student(self, student):
        if len(self.students) < self.size_group:
            self.students.append(student)
            student.group = self.group
            return True
        else:
            return f"This grup already has {self.size_group} students"

            
class Students(People_in_school):
    def __init__(self, profession, name, age):
        super().__init__(profession, name, age)


school1 = School("Primary school", 5, "Wroclaw") # szkoła 
school2 = School("Primary school", 3, "Warsaw")

biology_room = Classroom("Biology room", 102, school1) # sala 
chemistry_room = Classroom("Chemistry room", 121, school1)
gym_room = Classroom("Gym", 10, school2)

teacher1 = Teacher("Teacher", "Jan Kowalski", 47) # nauczyciel

group_1a = Group_of_students(1, "a", 20, teacher1) # klasa

student1 = Students("Student", "Alicja Kot", 10)
student2 = Students("Student", "Karol Urbański", 10)
 


print(biology_room.speak())
print(gym_room.speak())

print(isinstance(teacher1, Teacher)) 
print(group_1a.group_description())

print(student1.introduce())
group_1a.add_student(student1)
print(group_1a.group_description())

biology_room.add_group(group_1a)
print(biology_room.classroom_description())
