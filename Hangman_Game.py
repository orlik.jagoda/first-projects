import random

hangman_list_picture = [
"""
        / \  """,
"""
        |        
        |        
        |        
        |        
        |        
       / \  """,
"""
        ------  
        |       
        |       
        |       
        |       
        |       
       / \  """,
"""
        ------
        |     O     
        |
        |       
        |           
        |
       / \  """,
"""
        ------  
        |     O 
        |     |   
        |     |   
        |       
        |       
       / \   """,
"""
        ------  
        |     x     
        |    /|\    
        |     |     
        |    / \    
        |           
       / \   """
]

categories = ["1 - fruit", "2 - vegetables", "3 - items"]
fruit = ["apple", "banana", "orange", "strawberry"]
vegetables = ["cucumber", "tomato", "carrot", "onion"]
items = ["car", "pencil", "telephone" "house"]

question_string = """Do you know answer?:
    Y - Yes, I know the answer.
    N - No, I need more letter.
    X - Exit, I'm finishing the game
: """ 

def hangman(categorie, picture):
    n = 0
    word = (random.choices(categorie)[0]).upper()
    word_len, word_list = len(word), list(word)
    guess_list = list(('_ ' * word_len).split())
    print(f"Categoties fruit")
    print(f"The word you are looking for has {word_len} letters")
    print( )
    print(guess_list)
    while True:
        print()
        question = str((input(question_string)).upper())
        if question == "Y":
            answer = (input("Enter answer: ")).upper()
            if answer == word:
                print( )
                print("WELL DONE, YOU WIN !!!")
                print()
                exit()
            else:
                print("GAME OVER")
                exit()
        elif question == "N":
            user_letter = (input("enter the letter: ")).upper()
            if user_letter in word_list:
                for i in enumerate(word_list):
                    if i[1] == user_letter:
                        index = int(i[0])
                        guess_list[index] = user_letter
                print(guess_list)             
            else:
                if n == (len(picture)-1):
                    print(picture[n])
                    print("GAME OVER")
                    break
                else:
                    print(picture[n])
                    n = n + 1
        elif question == "X":
            print("Game over")
            exit()
        else:
            print("Try again")
            continue


print("""
|----------------------------------------------|
|----------------HANGMAN GAME------------------|
|----------------------------------------------|
""")
print("""
GAME RULES
Your job is to find the word I'm thinking about.
You can enter one letter in each round. You can
guess until you make 6 mistakes.
Have fun !!!
""")

user_categories = int(input(f"Select category number {categories}:  "))
for i in range(3):
    if user_categories == 1:
        hangman(fruit, hangman_list_picture)
    elif user_categories == 2:
        hangman(vegetables, hangman_list_picture)
    elif user_categories == 3:
        hangman(items, hangman_list_picture)    
    else:
        print("Try again")
        continue
